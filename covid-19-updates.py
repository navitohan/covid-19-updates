import os
import sys
import time
import json
import pathlib
from io import StringIO

pwd = pathlib.Path(__file__).parent.absolute()
    filename = str(pwd) + "/covid-19-updates.log",
    format = '%(asctime)s %(levelname)-8s %(message)s',
    level = logging.DEBUG,
    datefmt = '%Y-%m-%d %H:%M:%S')

if sys.platform.startswith('linux'):
    import notify2
    notify2.init('Covid-19 updates')
elif sys.platform.startswith('win32'):
    from plyer import notification
elif sys.platform.startswith('darwin'):
    from pync import Notifier as notification
else:
    logging.error("platform not supported")
    exit()

logging.info("***********************")
logging.info("starting with pid: %d", os.getpid())
logging.info("***********************")
config_file_path=str(pwd) + "/config.json"
if os.path.isfile(config_file_path):
    with open(config_file_path) as json_file:
        config=json.loads(json_file.read())
else:
    logging.error("config file not found")
    exit()

update_countries=[]
interval_in_seconds=1
try:
    interval_in_minutes=int(config["interval_in_minutes"])
    if interval_in_minutes > 1:
        interval_in_seconds=interval_in_minutes * 60
        if interval_in_seconds < 1:
            interval_in_seconds=1
    else:
        interval_in_minutes=1
    for countries in config["countries"]:
        update_countries.append(countries)
except:
    logging.error("failed to read config" + json.dumps(config))
    exit()

if not update_countries:
    logging.warning(
        "failed to read countires from config, setting Germnay as default")
    update_countries.append("Germany")

URL='https://www.worldometers.info/coronavirus/'
options=webdriver.ChromeOptions()
options.add_argument('headless')
driver=webdriver.Chrome(options = options)

# Country, Total Cases, New Cases, Total Deaths, New Deaths, Total Recoveries, Active Cases, Serious Cases
countries={}


def parse(values):
    countries.clear()
    for value in StringIO(values):
        v=value.split()
        if len(v) > 5:
            total_cases=v[1]
            new_cases=v[2]
            deaths=v[3]
            # new cases and new deaths are not always reported
            if new_cases.find('+') == -1:
                deaths=new_cases
                new_cases=str(0)
                new_deaths=v[3]
                total_recovered=v[4]
            else:
                new_deaths=v[4]
                total_recovered=v[5]
            if new_deaths.find('+') == -1:
                total_recovered=new_deaths
                new_deaths=str(0)
            countries[v[0]]=(total_cases, new_cases, deaths,
                               new_deaths, total_recovered)


def getUpdate():
    logging.info("updating...")
    driver.get(URL)
    p_element=driver.find_element_by_id(id_ = 'main_table_countries_today')
    parse(p_element.text)


def update(country):
    c=countries[country]
    title="Covid-19 update: " + country
    msg="Total: " + c[0] +
        " New: " + c[1] + " Deaths: " + c[2] + " Cured: " + c[4]
    logging.info(title)
    logging.info(msg)
    if sys.platform.startswith('linux'):
        n=notify2.Notification(title, msg)
        n.set_timeout(interval_in_seconds * 1000)
        n.show()
    elif sys.platform.startswith('win32'):
        notification.notify(title = title,
                            message=msg,
                            app_icon=None,
                            timeout=interval_in_seconds)
    elif sys.platform.startswith('darwin'):
        notification.notify(msg, title=title)
    else:
        logging.error("platform not supported")


if __name__ == "__main__":
    while True:
        try:
            getUpdate()
            for country in update_countries:
                update(country)
        except:
            logging.error("an exception occurred")
        time.sleep(interval_in_seconds)
        if sys.platform.startswith('darwin'):
            notification.remove()
