# covid-19-updates

covid-19-updates is a python based lightweight tool to get updates on Covid-19.


## Supported OS
- MacOS
- Ubuntu 18.04
- Windows 10

## Launch
```
git clone https://gitlab.com/navitohan/covid-19-updates.git
cd covid-19-updates
python3 covid-19-updates.py &
```

## Config
- interval_in_minutes [default=30m]: edit to change interval of updates
- countries: add/remove country list

## Dependecies
### On Ubuntu
```
sudo apt install chromium-chromedriver python3-pip python3-dbus
pip3 install notify2
pip3 install selenium
```
### On Windows
```
pip3 install plyer
pip3 install selenium
```

**Add chromedriver path C:\Users\<checkout_path>\covid-19-updates\win to [environment variables](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/)**

### On MacOS
```
pip3 install pync
pip3 install selenium
echo "export PATH=<checkout_path>/covid-19-updates/mac:$PATH" >> ~/.bashrc
```


## Future work
- Create debian package
- Create Windows installer
- Add Android support

## Output
### On Unbuntu
![](img/covid-19-screenshort.png)
### On Windows
![](img/covid-19-screenshort_win.png)
### On MacOS
![](img/covid-19-screenshort_mac.png)